# Microservices workflow

🎯 __Mission statement.__ Demonstrate a way to develop, orchestrate, and
deploy microservices using GitLab.

Here's a diagram with the overall architecture of our Continuous
Integration/Continuous Deployment (CI/CD) system at Vivalerts.

![Microservices workflow](Workflow.svg)

Our site consists of three microservices (Docker containers)
orchestrated by Docker Compose.

The `docker-compose.yml` specification is stored in the Stack
repository. The code and the `Dockerfile`s for the microservices are
stored in the Crawler, Site Rendering, and Proxy repositories.

The component-level testing may include coding style checks, static code
analysis, and unit tests.

To build the Docker images, we may use the already available GitLab's
`Dockerfile`s templates for the respective programming language.

The GitLab CI job for the Stack pipeline is triggered whenever commits
are pushed to this repository or the upstream-microservices
repositories. The site is deployed in an isolated volatile Docker
environment, and an end-to-end test is performed.

Then, the Ansible script inside the open source
`deploy-docker-compose-app` image deploys the site on a remote virtual
machine.

## Skipping CI or deployment

If you don't want your changes to trigger the CI system, you may execute

```
git push -o ci.skip
```

If you want the CI to run but don't want your changes to be deployed on
a remote machine, you may execute

```
git push -o ci.variable=SKIP_DEPLOYMENT=TRUE
```

## Feature branches

 - Whenever you push a change to the `main` branch of a microservice or
   Stack repository, it'll eventually be deployed into the production
   virtual machine.

 - Whenever you push a change to a feature branch, the CI system
   combines the changes with the `main` branches of the other
   repositories and deploys them to the `testing.vivalerts.com` virtual
   machine.

## Combining changes with other non-`main`  branches

Whenever you push a change to the `main` branch of a microservice
repository, the respective Docker image with the `latest` tag is
created. On the other hand, if you push a change to the `xyz` feature
branch, a Docker image with the tag `xyz` will be automatically created.

Let's suppose that in the current directory you've checked out the `xyz`
feature branch of the Site Rendering microservice. If you need for
example to combine the changes of the Site Rendering `xyz` branch with
the `abc` feature branch of the Stack, you can execute

```
git push -o ci.variable=TAG_STACK=abc
```

If you also need to combine the changes with the `def` branch of the
Crawler microservice, you can execute

```
git push -o ci.variable=TAG_STACK=abc -o ci.variable=TAG_CRAWLER=def
```
